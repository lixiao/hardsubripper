#include "framon.h"

void Framon::eat(PIX *frame)
{
    PIX*filled=pixCreate(frame->w,frame->h,1);
    for(int x=0;x<frame->w;++x)
    {
        for(int y=0;y<frame->h;++y)
        {
            l_uint32 pp;
            pixGetPixel(filled,x,y,&pp);
            if(!pp){
                int pr,pg,pb;
                pixGetRGBPixel(frame,x,y,&pr,&pg,&pb);
                if(abs(dr-pr)+abs(dg-pg)+abs(db-pb)<delta)
                {
                    std::stack<PNT>points;
                    points.push({x,y});
                    pixSetPixel(filled,x,y,1);
                    while(!points.empty())
                    {
                        int px=points.top().x,py=points.top().y;
                        points.pop();
                        if(px>0)
                        {
                            pixGetPixel(filled,px-1,py,&pp);
                            if(!pp)
                            {
                                pixGetRGBPixel(frame,px-1,py,&pr,&pg,&pb);
                                if(abs(dr-pr)+abs(dg-pg)+abs(db-pb)<threshold)
                                {
                                    points.push({px-1,py});
                                    pixSetPixel(filled,px-1,py,1);
                                }
                            }
                        }
                        if(px<frame->w-1)
                        {
                            pixGetPixel(filled,px+1,py,&pp);
                            if(!pp)
                            {
                                pixGetRGBPixel(frame,px+1,py,&pr,&pg,&pb);
                                if(abs(dr-pr)+abs(dg-pg)+abs(db-pb)<threshold)
                                {
                                    points.push({px+1,py});
                                    pixSetPixel(filled,px+1,py,1);
                                }
                            }
                        }
                        if(py>0)
                        {
                            pixGetPixel(filled,px,py-1,&pp);
                            if(!pp)
                            {
                                pixGetRGBPixel(frame,px,py-1,&pr,&pg,&pb);
                                if(abs(dr-pr)+abs(dg-pg)+abs(db-pb)<threshold)
                                {
                                    points.push({px,py-1});
                                    pixSetPixel(filled,px,py-1,1);
                                }
                            }
                        }
                        if(py<frame->h-1)
                        {
                            pixGetPixel(filled,px,py+1,&pp);
                            if(!pp)
                            {
                                pixGetRGBPixel(frame,px,py+1,&pr,&pg,&pb);
                                if(abs(dr-pr)+abs(dg-pg)+abs(db-pb)<threshold)
                                {
                                    points.push({px,py+1});
                                    pixSetPixel(filled,px,py+1,1);
                                }
                            }
                        }

                    }
                }
            }
        }
    }
}
