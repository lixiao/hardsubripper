#ifndef FRAMON_H
#define FRAMON_H

#include<stack>
extern "C"
{
#include<allheaders.h>
}

typedef struct point
{
    int x;
    int y;
}PNT;

class Framon
{
    l_int32 dr,dg,db,delta,threshold;
public:
    Framon(l_int32 favouriter,l_int32 favouriteg,l_int32 favouriteb,l_int32 sensitivity,l_int32 endurance)
    {
        dr=favouriter;
        dg=favouriteg;
        db=favouriteb;
        delta=sensitivity;
        threshold=endurance;
    }
    void eat(PIX*frame);
};

#endif // FRAMON_H
