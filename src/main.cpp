#include <stdio.h>
extern "C"
{
#include<libavcodec/avcodec.h>
#include<libavformat/avformat.h>
#include<libswscale/swscale.h>
#include<allheaders.h>
}
#include<tesseract/baseapi.h>
#include<cmath>
#include<stack>
#include"framon.h"

typedef struct subtitleRow
{
    int sFrame;
    int eFrame;
    PIXA components;
}subrow;


int main(int argc,char**argv)
{
    av_register_all();
    AVFormatContext*pFormatCtx=NULL;
    if(avformat_open_input(&pFormatCtx, argv[1], NULL, NULL)!=0)
        return -1;
    if(avformat_find_stream_info(pFormatCtx,NULL)<0)
        return -2;
    av_dump_format(pFormatCtx,0,argv[1],0);

    int videostream=-1,i=0;
    for(;i<pFormatCtx->nb_streams;++i)
    {
        if(pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO)
        {
            videostream=i;
            break;
        }
    }
    if(videostream==-1)
        return -3;
    AVCodecContext *pCodecCtx=pFormatCtx->streams[i]->codec;

    AVCodec*pCodec=avcodec_find_decoder(pCodecCtx->codec_id);
    if(pCodec==NULL)
    {
        fprintf(stderr, "Unsupported codec!\n");
        return -4;
    }
    if(avcodec_open2(pCodecCtx, pCodec,NULL)<0)
        return -5;

    uint8_t *buffer;
    int numBytes=avpicture_get_size(PIX_FMT_RGB24,pCodecCtx->width,pCodecCtx->height);
    buffer=(uint8_t *)av_malloc(numBytes*sizeof(uint8_t));
    AVFrame*pFrameRGB=avcodec_alloc_frame();
    avpicture_fill((AVPicture *)pFrameRGB, buffer, PIX_FMT_RGB24,pCodecCtx->width, pCodecCtx->height);

    int frameFinished;
    AVPacket packet;
    av_init_packet(&packet);
    AVFrame*pFrame=avcodec_alloc_frame();
    struct SwsContext*img_convert_ctx;
    img_convert_ctx = sws_getContext(pCodecCtx->width, pCodecCtx->height, pCodecCtx->pix_fmt,pCodecCtx->width, pCodecCtx->height, PIX_FMT_RGB32_1, SWS_BICUBIC, NULL, NULL, NULL);
    if(img_convert_ctx == NULL)
    {
        fprintf(stderr, "Cannot initialize the conversion context!\n");
        return -6;
    }

    tesseract::TessBaseAPI*api=new tesseract::TessBaseAPI;
    if(api->Init(NULL,"eng"))
    {
        fprintf(stderr, "Could not initialize tesseract.\n");
        return -7;
    }

    i=0;
    while(av_read_frame(pFormatCtx,&packet)>=0)
    {
        if(packet.stream_index==videostream)
        {
            avcodec_decode_video2(pCodecCtx,pFrame,&frameFinished,&packet);
            if(frameFinished)
            {
                sws_scale(img_convert_ctx, pFrame->data, pFrame->linesize, 0, pCodecCtx->height, pFrameRGB->data, pFrameRGB->linesize);
                if(++i<200)
                {
                    PIX*a=(struct Pix*)malloc(sizeof(struct Pix));
                    a->w=pCodecCtx->width;
                    a->h=pCodecCtx->height;
                    a->d=32;
                    a->wpl=pFrameRGB->linesize[0]/4;
                    a->colormap=NULL;
                    a->refcount=1;
                    a->data=reinterpret_cast<l_uint32*>(pFrameRGB->data[0]);
                    PIX*b=pixCreate(a->w,a->h,1);
                    Framon mon(255,255,255,2,30);
                    mon.eat(a);
                    pixWrite("pFrame",b,IFF_PNG);
                    api->SetImage(b);
                    char*text=api->GetUTF8Text();
                    printf(text);
                    delete[]text;
                    free(a);
                    pixDestroy(&b);
                }
            }
        }
        av_free_packet(&packet);
    }

    api->Clear();
    api->End();

    av_free(buffer);
    av_free(pFrameRGB);
    av_free(pFrame);
    avcodec_close(pCodecCtx);
    av_close_input_file(pFormatCtx);
    printf("%d\n",i);
    return 0;
}

